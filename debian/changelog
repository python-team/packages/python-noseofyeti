python-noseofyeti (2.4.9-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.7.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 11 May 2024 13:11:39 -0400

python-noseofyeti (2.4.8-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 16 Mar 2024 16:03:42 -0400

python-noseofyeti (2.4.7-1) unstable; urgency=medium

  * New upstream release
  * Add Lintian override: package-contains-documentation-outside-usr-share-doc

 -- Scott Kitterman <scott@kitterman.com>  Mon, 19 Feb 2024 14:40:32 -0500

python-noseofyeti (2.4.6-1) unstable; urgency=medium

  * Update d/watch for new upstream tarball naming convention
  * Add Rules-Requires-Root no to d/control
  * New upstream release
  * Replace python3-setuptools with pybuild-plugin-pyproject and
    python3-hatchling in Build-Depends
  * Update d/control with wrap-and-sort
  * Override dh_auto_test to not run since tests require unpackaged modules

 -- Scott Kitterman <scott@kitterman.com>  Sat, 20 Jan 2024 23:07:59 -0500

python-noseofyeti (2.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 27 Jun 2023 00:02:44 -0400

python-noseofyeti (2.4.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.6.2 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 20 Dec 2022 12:00:56 -0500

python-noseofyeti (2.4.0-1) unstable; urgency=medium

  * New upstream release
  * Update d/rules to remove stray noy_black.pth file

 -- Scott Kitterman <scott@kitterman.com>  Mon, 21 Nov 2022 13:52:01 -0500

python-noseofyeti (2.3.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-all.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 14:52:14 +0000

python-noseofyeti (2.3.1-2) unstable; urgency=medium

  * Source upload to allow binary to be built on buildd
  * Correct repository path for Vcs-* fields in debian/control.

 -- Scott Kitterman <scott@kitterman.com>  Tue, 18 Oct 2022 20:15:43 -0400

python-noseofyeti (2.3.1-1) unstable; urgency=low

  * Initial release (Closes: #1021964)
    - Do not install /usr/bin scripts since they aren't needed to run testts

 -- Scott Kitterman <scott@kitterman.com>  Mon, 17 Oct 2022 20:45:26 -0400
